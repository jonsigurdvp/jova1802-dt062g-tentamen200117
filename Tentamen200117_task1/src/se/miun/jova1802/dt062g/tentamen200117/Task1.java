package se.miun.jova1802.dt062g.tentamen200117;

import java.io.FileWriter;
import java.io.IOException;

import javax.swing.SwingUtilities;

/**
* <h1>Final Exam</h1>
* This class is the starting point for the application.
*
* @author  J�n Sigurd Vang Poulsen (jova1802)
* @version 1.0
* @since   17/01/2020
*/
public class Task1
{
	/**
	 * launching app
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException
	{
		String test = "A scientific study done at a university in England has shown that if the \n"
				+ "first and last letter of all words in a text are properly placed, it does \n"
				+ "not matter in what order the other letters of the words come. The text is \n"
				+ "fully readable even if the other letters come higgledy-piggledy.";
		
		String out = WordRandomizer.randomize(test);
		
		WordRandomizerFilterWriter write = new WordRandomizerFilterWriter(new FileWriter("test.txt"));
		write.write(out, 0, test.length());
		write.close();
		
		char[] cbuf = test.toCharArray();
		WordRandomizerFilterWriter write_1 = new WordRandomizerFilterWriter(new FileWriter("test_1.txt"));
		write_1.write(cbuf, 0, 45);
		write_1.close();
		
		WordRandomizerFilterWriter write_2 = new WordRandomizerFilterWriter(new FileWriter("test_2.txt"));
		write_2.write(WordRandomizer.randomize(test), 5, 69);
		write_2.close();
	}
}