package se.miun.jova1802.dt062g.tentamen200117;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

/**
* This class takes care of the writing to file
* 
* @author  J�n Sigurd Vang Poulsen (jova1802)
* @version 1.0
* @since   17/01/2020
*/
public class WordRandomizerFilterWriter extends FilterWriter
{
	//-------------------------------------------------
	//	Constructor
	//--------------------------------------------begin
	
	/**
	 * Constructor that takes one parameter
	 * @param out
	 */
    public WordRandomizerFilterWriter(Writer out)
    {
        super(out);
    }
    
    //----------------------------------------------end
    
    
    //-------------------------------------------------
  	//	Public void methods
  	//--------------------------------------------begin
    
    /**
     * This writer takes a string as parameter
     */
    public void write(String str) throws IOException
    {
        this.write(str, 0, str.length());
    }
    
    /**
     * This writer takes char array and ints as parameters
     */
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException
    {
        write(new String(cbuf), off, len);
    }
    
    /**
     * This writer takes a string and ints as parameters
     */
    @Override
    public void write(String str, int off, int len) throws IOException
    {
        super.write(WordRandomizer.randomize(str), off, len);
    }
	
	//----------------------------------------------end
}


