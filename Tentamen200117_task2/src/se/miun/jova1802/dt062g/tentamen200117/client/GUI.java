package se.miun.jova1802.dt062g.tentamen200117.client;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Toolkit;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.EventObject;
import java.awt.Insets;

/**
* This class is the JFrame that displays our GUI containing all of the
* components that I have programmed to fit our needs until this point
*
* @author  J�n Sigurd Vang Poulsen (jova1802)
* @version 1.0
* @since   15/01/2020
*/
public class GUI extends JFrame
{
	//-------------------------------------------------
	//	Private data members
	//--------------------------------------------begin
	
	private static GUI frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnConnect;
	private JButton btnDisconnect;
	
	//----------------------------------------------end
	
	
	//-------------------------------------------------
	//	Public static method
	//--------------------------------------------begin
	
	/**
	 * Launching the application
	 * I've made this main method in order to make sure the input dialogs appear in center of the frame,
	 * when referring to the instance variable of the GUI
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			/**
			 * public void method that runs the application, when the runnable event occurs
			 */
			public void run()
			{
				try
				{
					frame = new GUI();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	//----------------------------------------------end
	
	
	//-------------------------------------------------
	//	Constructor
	//--------------------------------------------begin
	
	/**
	 * Constructor that is the whole frame itself. It sets the icon image,
	 * closing operation, sets the size of the frame and calls the public
	 * methods that make up the frame
	 */
	public GUI()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1080, 720);
		
		setTitle("Client");
		contentPane();
	}
	
	//----------------------------------------------end
	
	class JEvent implements ActionListener
	{	
		public void actionPerformed(ActionEvent ae)
		{
			if (((EventObject) ae).getSource() == btnConnect)
			{
				if (textField_1.getText() != null)
				{
					String prtNr = textField_1.getText();
					int pNr = Integer.parseInt(prtNr);
					
					if (pNr < 0 && pNr > 65535)
					{
						JOptionPane.showMessageDialog
						(
							frame,
							"The port number is out of range!",
							"Port Number Invalid",
							getDefaultCloseOperation()
						);
					}
				}
			}
		}
	}
	
	//-------------------------------------------------
	//	Public method
	//--------------------------------------------begin
	
	/**
	 * public void method that creates the content pane and all of
	 * its components
	 */
	public void contentPane()
	{	
		JPanel contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);				
		
		JPanel toolbar = new JPanel();
		toolbar.setLayout(new GridLayout(0, 6, 0, 0));
		toolbar.setPreferredSize(new Dimension(this.getWidth(), 50));
		GridBagConstraints gbc_toolbar = new GridBagConstraints();
		gbc_toolbar.fill = GridBagConstraints.BOTH;				
		gbc_toolbar.gridy = 0;									
		gbc_toolbar.gridx = 0;									
		contentPane.add(toolbar, BorderLayout.PAGE_START);
		
		JLabel lblAddress = new JLabel("Address:");
		toolbar.add(lblAddress);
		
		textField_1 = new JTextField();
		toolbar.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPort = new JLabel("Port:");
		toolbar.add(lblPort);
		
		textField_2 = new JTextField();
		toolbar.add(textField_2);
		textField_2.setColumns(10);
		
		btnConnect = new JButton("Connect");
		toolbar.add(btnConnect);
		btnConnect.addActionListener(new JEvent());
		
		btnDisconnect = new JButton("Disconnect");
		toolbar.add(btnDisconnect);
		
		JPanel outputMsg = new JPanel();
		contentPane.add(outputMsg, BorderLayout.CENTER);
		
		JPanel statusPanel = new JPanel();
		statusPanel.setBackground(Color.WHITE);
		GridBagLayout gbl_statusPanel = new GridBagLayout();			
		gbl_statusPanel.rowWeights = new double[]{1.0};					
		gbl_statusPanel.columnWeights = new double[]{1.0, 0.0};				
		statusPanel.setLayout(gbl_statusPanel);								
		contentPane.add(statusPanel, BorderLayout.PAGE_END);
		
		JButton send = new JButton("Send");
		GridBagConstraints gbc_send = new GridBagConstraints();	
		gbc_send.fill = GridBagConstraints.VERTICAL;					
		gbc_send.gridy = 0;													
		gbc_send.gridx = 1;														
		statusPanel.add(send, gbc_send);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 0, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 0;
		gbc_textField_2.gridy = 0;
		statusPanel.add(textField, gbc_textField_2);
		textField.setColumns(10);
	}
	
	//----------------------------------------------end
}
