package se.miun.jova1802.dt062g.tentamen200117.client;

import javax.swing.SwingUtilities;

import se.miun.jova1802.dt062g.tentamen200117.client.GUI;

/**
* <h1>Final Exam</h1>
* This class is the starting point for the client.
*
* @author  J�n Sigurd Vang Poulsen (jova1802)
* @version 1.0
* @since   17/01/2020
*/
public class Client
{
	/**
	 * launching app
	 * @param args
	 */
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new GUI().setVisible(true);
			}
		});	
	}
}