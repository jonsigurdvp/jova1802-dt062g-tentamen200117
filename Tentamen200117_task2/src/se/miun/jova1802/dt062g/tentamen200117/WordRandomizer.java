package se.miun.jova1802.dt062g.tentamen200117;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
* This class takes care of the randomization of the test string
* 
* @author  J�n Sigurd Vang Poulsen (jova1802)
* @version 1.0
* @since   17/01/2020
*/
abstract class WordRandomizer
{
	// Private data member
	private static ArrayList<String> random;
	
	//-------------------------------------------------
	//	Constructor
	//--------------------------------------------begin
	
	/**
	 * Default constructor that creates a new instance of array list random
	 */
	public WordRandomizer()
	{
		random = new ArrayList<String>();
	}
	
	//----------------------------------------------end
	
	
	//-------------------------------------------------
	//	Public void methods
	//--------------------------------------------begin
	
	/**
	 * public static String method that shuffles the letters within a string
	 * @param string
	 * @return string shuffled
	 */
	public static String shuffleString(String string)
	{
		List<String> letters = Arrays.asList(string.split(""));	// Splitting for each character in the string
		Collections.shuffle(letters);	// Shuffling the letters
		String shuffled = "";
		for(String letter : letters)
		{
			shuffled += letter;	// Adding the shuffled letters to the string
		}
		
		return shuffled;	// Returning string to the randomize function that made the call
	}
	
	/**
	 * public static String method that takes a string an splits it to
	 * make sure that only the letters in the middle of a word are randomized
	 * and returns the complete string containing words that are completely
	 * randomized, except from the first and last letter in each word
	 * @param str
	 * @return string result
	 */
	public static String randomize(String str)
	{   
        String[] split = str.split(" ");	// Splitting string for every occurrence of space
        ArrayList<String> random = new ArrayList<String>();
        String randomized;
        
        for (String s : split)
        {
        	String middle;
        	String first;
        	String last;
        	
        	// if else statements that decide what to do with the splitted string
        	if (s.length() <= 3)
    		{
    			random.add(s + " ");
    		}
    		else if (s.contains(".,:;!?"))
    		{
    			// Deciding on how much of the splitted string should be part of the first middle and last, if it contains special characters
    			first = s.substring(0,1);	// first element
    			middle = s.substring(1, s.length() - 2);	// 2nd to 3rd last element
            	last = s.substring(s.length() - 2, s.length());	// 3rd last to last element
    			
    			randomized = first + shuffleString(middle) + last + " ";	// Adding first, the shuffled middle and last to the randomized string
            	
            	random.add(randomized);	// Adding the randomized string to the array list
    		}
    		else if (s.contains("-"))
    		{
    			first = s.substring(0,1);
    			middle = s.substring(1, s.length() - 1);	// all excluding first and last
            	last = s.substring(s.length() - 1, s.length());
            	
    			String[] inner_split = s.split("-");
    			int count = 1;
    			
    			// Splitting the splitted string, if it contains "-"
    			for (String ss : inner_split)
    			{	
    				String middle_1 = ss.substring(1, ss.length() - 1);
    	        	String first_1 = ss.substring(0,1);
    	        	String last_1 = ss.substring(ss.length() - 1, ss.length());
    	        	
    	        	if (count == 2 && ss.contains(".") || ss.contains(",") || ss.contains(":") || ss.contains(";") || ss.contains("!") || ss.contains("?"))
    	        	{
    	        		middle_1 = ss.substring(1, ss.length() - 2);
    	            	last_1 = ss.substring(ss.length() - 2, ss.length());
    	            	
    	        		randomized = first_1 + shuffleString(middle_1) + last_1 + " ";
    	        	}
    	        	else
    	        	{
    	        		randomized = first_1 + shuffleString(middle_1) + last_1 + "-";
    	        	}
    	        	
                	random.add(randomized);
                	
                	count++;
    			}
    		}
    		else
    		{
    			first = s.substring(0,1);
    			middle = s.substring(1, s.length() - 2);
            	last = s.substring(s.length() - 2, s.length());
            	
    			randomized = first + shuffleString(middle) + last + " ";
            	
            	random.add(randomized);
    		}
        }
        
        String result = "";
        
        // Adding the contents of random to the result string
        for (String e : random)
        {
        	result += e;
        }
        
		return result;
	}
	
	//----------------------------------------------end
}
